const state = () => ({
  currentCrypto: null
})

const getters = {
  CURRENT_CRYPTO: state => state.currentCrypto
}

const mutations = {
  SET_CURRENT_CRYPTO: (state, cryptoName) => {
    state.currentCrypto = cryptoName
  }
}

const moduleStore = {
  state, getters, mutations
}

export default moduleStore
  
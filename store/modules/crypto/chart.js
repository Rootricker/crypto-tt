const state = () => ({
  currentCryptoFullData: []
})

const getters = {
  CURRENT_CRYPTO_FULL_DATA: state => state.currentCryptoFullData
}

const mutations = {
  SET_CURRENT_CRYPTO_FULL_DATA: (state, newFullData) => {
    state.currentCryptoFullData = [ ...newFullData ]
  },
  ADD_NEW_HISTORY_DATA_TO_CURRENT_CRYPTO: (state, newHistory) => {
    state.currentCryptoFullData.push(newHistory)
  }
}

const moduleStore = {
  state, getters, mutations
}

export default moduleStore

const state = () => ({
  cryptoTopList: null,
  cryptoPriceData: null
})

const getters = {
  CRYPTO_TOP_LIST: state => state.cryptoTopList,
  CRYPTO_PRICE_DATA: state => state.cryptoPriceData
}

const mutations = {
  SET_CRYPTO_TOP_LIST: (state, newTopList) => {
    state.cryptoTopList = [ ...newTopList ]
  },
  SET_CRYPTO_PRICE_DATA: (state, newPriceData) => {
    state.cryptoPriceData = { ...newPriceData }
  },
  SET_CURRENT_CRYPTO_PRICE_DATA: (state, { cryptoName, cryptoPrice }) => {
    const cryptoPriceDataClone = { ...state.cryptoPriceData }
    cryptoPriceDataClone[cryptoName].USD = +cryptoPrice

    state.cryptoPriceData = { ...cryptoPriceDataClone }
  }
}

const actions = {
  async GET_CRYPTO_TOP_LIST ({ commit }, { limit, tsym }) {
    let res

    try {

      res = await this.$axios.get(`${this.$axios.defaults.baseURL}/data/top/totalvolfull`, {
        params: {
          limit,
          tsym
        }
      })
      const data = await res.data
      
      const topList = data.Data.map(item => {
        return item.CoinInfo.Name
      })

      commit('SET_CRYPTO_TOP_LIST', topList)

    } catch (err) {
      throw new Error(err)
    }

    return res
  },
  async GET_CRYPTO_PRICE_DATA ({ getters, commit }, { fsyms, tsyms }) {
    let res

    try {

      res = await this.$axios.get(`${this.$axios.defaults.baseURL}/data/pricemulti`, {
        params: {
          fsyms,
          tsyms
        }
      })
      const data = await res.data
      commit('SET_CRYPTO_PRICE_DATA', data)

    } catch (err) {
      throw new Error(err)
    }

    return res
  }
}

const moduleStore = {
  state, getters, mutations, actions
}

export default moduleStore

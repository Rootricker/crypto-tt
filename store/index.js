import Vue from 'vue'
import Vuex from 'vuex'

import CryptoTable from '~/store/modules/crypto/table'
import CryptoChart from '~/store/modules/crypto/chart'
import CryptoCurrent from '~/store/modules/crypto/current'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = () => new Vuex.Store({
  modules: {
    CryptoTable,
    CryptoChart,
    CryptoCurrent
  },
  strict: debug
})

export default store
